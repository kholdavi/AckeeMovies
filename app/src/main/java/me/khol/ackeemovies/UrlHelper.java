package me.khol.ackeemovies;



public class UrlHelper {

	private static final String API_KEY_PARAM = "api_key=3defbee8a7ff35deff02366f0f76a940";

	private static final String POPULAR_MOVIES = "http://api.themoviedb.org/3/movie/popular";
	private static final String POPULAR_MOVIES_MOCK = "http://private-anon-507350d09-themoviedb.apiary-mock.com/3/movie/popular";

	private static final String MOVIE = "http://api.themoviedb.org/3/movie/";
	private static final String MOVIE_MOCK = "http://private-anon-ef583ef2b-themoviedb.apiary-mock.com/3/movie/";

	private static final String IMAGE = "http://image.tmdb.org/t/p/";


	public enum PosterSize {
		W92("w92"),
		W154("w154"),
		W185("w185"),
		W342("w342"),
		W500("w500"),
		W780("w780"),
		ORIGINAL("original");

		private String size;

		PosterSize(String size) {
			this.size = size;
		}

		@Override
		public String toString() {
			return size;
		}
	}

	public enum BackdropSize {
		W300("w300"),
		W780("w780"),
		W1280("w1280"),
		ORIGINAL("original");

		private String size;

		BackdropSize(String size) {
			this.size = size;
		}

		@Override
		public String toString() {
			return size;
		}
	}



	public static String getPopularMoviesUrl(int page) {
		return POPULAR_MOVIES + "?" + API_KEY_PARAM + "&page=" + page;
	}

	public static String getMovieUrl(int movieId) {
		return MOVIE + movieId + "?" + API_KEY_PARAM;
	}

	public static String getPosterUrl(PosterSize size, String posterUrl) {
		return IMAGE + size + posterUrl;
	}

	public static String getBackdropUrl(BackdropSize size, String backdropUrl) {
		return IMAGE + size + backdropUrl;
	}



}
