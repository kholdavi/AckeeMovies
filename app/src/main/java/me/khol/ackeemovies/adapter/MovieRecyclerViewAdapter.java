package me.khol.ackeemovies.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;

import java.util.List;

import me.khol.ackeemovies.AppController;
import me.khol.ackeemovies.UrlHelper;
import me.khol.ackeemovies.R;
import me.khol.ackeemovies.model.Movie;


public class MovieRecyclerViewAdapter extends RecyclerView.Adapter<MovieRecyclerViewAdapter.ViewHolder> {

	private Context context;
	private final List<Movie> movies;

	public MovieRecyclerViewAdapter(Context context, List<Movie> movies) {
		this.context = context;
		this.movies = movies;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		final Movie movie = movies.get(position);

		String voteAverage = String.format("%.1f", Double.parseDouble(movie.getVote_average()));
		holder.ratingTextView.setText(context.getString(R.string.rating, voteAverage));
		holder.nameTextView.setText(String.valueOf(movie.getTitle()));

		holder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				itemClickListener.onAdapterItemClicked(v, movie);
			}
		});


		loadPoster(movie, holder.logoImageView);
	}

	private void loadPoster(final Movie movie, final ImageView view) {
		String url = UrlHelper.getPosterUrl(UrlHelper.PosterSize.W185, movie.getPoster_path());

		// Using Volley's ImageLoader to load images in much faster than doing ImageRequest,
		// because of the BitmapCache that caches images in memory. ImageRequest and other
		// Requests cache only on disk and loading times for images are noticeable.
		// This way when scrolling, images that were recently loaded seem to be loaded
		// instantaneously.
		ImageLoader imageLoader = AppController.getInstance().getImageLoader();
		view.setImageBitmap(null);

		imageLoader.get(url, new ImageLoader.ImageListener() {
			@Override
			public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
				if (response.getBitmap() != null) {
					view.setImageBitmap(response.getBitmap());
				}
			}

			@Override
			public void onErrorResponse(VolleyError error) {
			}
		});

//		AppController.getInstance().cancelPendingRequests(view);
//		view.setImageBitmap(null);
//
//		ImageRequest request = new ImageRequest(url,
//				new Response.Listener<Bitmap>() {
//					@Override
//					public void onResponse(Bitmap bitmap) {
//						view.setImageBitmap(bitmap);
//					}
//				}, 0, 0, ImageView.ScaleType.CENTER_INSIDE, null,
//				new Response.ErrorListener() {
//					public void onErrorResponse(VolleyError error) {
//					}
//				});
//		request.setTag(view);
//
//		AppController.getInstance().addToRequestQueue(request, view);
	}

	@Override
	public int getItemCount() {
		return movies.size();
	}



	public class ViewHolder extends RecyclerView.ViewHolder {

		public final View view;
		public final ImageView logoImageView;
		public final TextView nameTextView;
		public final TextView ratingTextView;

		public ViewHolder(View view) {
			super(view);
			this.view = view;
			this.logoImageView = (ImageView) view.findViewById(R.id.logo_imageView);
			this.nameTextView = (TextView) view.findViewById(R.id.name_textView);
			this.ratingTextView = (TextView) view.findViewById(R.id.rating_textView);
		}

	}



	public void setOnItemClickedListener(OnItemClickedListener listener) {
		this.itemClickListener = (listener == null) ? dummyListener : listener;
	}

	private OnItemClickedListener itemClickListener = dummyListener;
	private static OnItemClickedListener dummyListener = new OnItemClickedListener() {
		@Override
		public void onAdapterItemClicked(View v, Movie movie) {
			// do nothing
		}
	};

	public interface OnItemClickedListener {
		void onAdapterItemClicked(View v, Movie movie);
	}

}
