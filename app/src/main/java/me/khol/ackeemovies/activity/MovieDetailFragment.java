package me.khol.ackeemovies.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.khol.ackeemovies.AppController;
import me.khol.ackeemovies.R;
import me.khol.ackeemovies.UrlHelper;
import me.khol.ackeemovies.model.Genre;
import me.khol.ackeemovies.model.MovieDetail;


public class MovieDetailFragment extends Fragment { // implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final String TAG = MovieDetailFragment.class.getSimpleName();

	public static final String ARG_MOVIE_ID = "movie_id";
	public static final String ARG_MOVIE_TITLE = "movie_title";

	private static final String KEY_MOVIE = "key_movie";


	private final SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	private final SimpleDateFormat displayDateFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);

	private int movieId;
	private String movieTitle;

	private MovieDetail movie;

	private ImageView backdropImageView;
	private CollapsingToolbarLayout toolbarLayout;

	private View contentView;
	private View emptyView;
	private TextView titleTextView;
	private TextView overviewTextView;
	private TextView runningTimeTextView;
	private TextView releaseDateTextView;
	private TextView taglineTextView;
	private TextView genresTextView;
	private ImageView posterImageView;

	private Snackbar noConnectionSnackbar;


	public static MovieDetailFragment newInstance(int itemId, String movieTitle) {
		MovieDetailFragment fragment = new MovieDetailFragment();

		Bundle arguments = new Bundle();
		arguments.putInt(MovieDetailFragment.ARG_MOVIE_ID, itemId);
		arguments.putString(MovieDetailFragment.ARG_MOVIE_TITLE, movieTitle);

		fragment.setArguments(arguments);

		return fragment;
	}


	public MovieDetailFragment() {
		// empty
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		movieId = getArguments().getInt(ARG_MOVIE_ID);
		movieTitle = getArguments().getString(ARG_MOVIE_TITLE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (noConnectionSnackbar != null)
			noConnectionSnackbar.dismiss();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.movie_detail, container, false);

		contentView = rootView.findViewById(R.id.content);
		emptyView = rootView.findViewById(R.id.empty_view);

		contentView.setVisibility(View.GONE);
		emptyView.setVisibility(View.GONE);


		overviewTextView = (TextView) rootView.findViewById(R.id.overview_textView);
		titleTextView = (TextView) rootView.findViewById(R.id.title_textView);
		releaseDateTextView = (TextView) rootView.findViewById(R.id.releaseDate_textView);
		runningTimeTextView = (TextView) rootView.findViewById(R.id.runningTime_textView);
		taglineTextView = (TextView) rootView.findViewById(R.id.tagline_textView);
		genresTextView = (TextView) rootView.findViewById(R.id.genres_textView);
		posterImageView = (ImageView) rootView.findViewById(R.id.poster_ImageView);

		toolbarLayout = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
		backdropImageView = (ImageView) getActivity().findViewById(R.id.backdropImage_imageView);

		titleTextView.setText(movieTitle);

		if (toolbarLayout != null) {
			toolbarLayout.setTitle(movieTitle);
		}


		if (savedInstanceState == null) {
			loadMovie(movieId);
		} else {
			movie = (MovieDetail) savedInstanceState.getSerializable(KEY_MOVIE);
			if (movie == null)
				loadMovie(movieId);
			else
				displayMovieView(movie);
		}

		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(KEY_MOVIE, movie);
	}

	private void loadMovie(final int movieId) {
		String url = UrlHelper.getMovieUrl(movieId);

		if (noConnectionSnackbar != null)
			noConnectionSnackbar.dismiss();

		AppController.getInstance().cancelPendingRequests(this);

		JsonObjectRequest request = new JsonObjectRequest(url,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Gson gson = new Gson();
						String json = response.toString();
						MovieDetail movie = gson.fromJson(json, MovieDetail.class);

						displayMovieView(movie);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						displayEmptyView();

						if (error.networkResponse == null) {
							noConnectionSnackbar = Snackbar.make(emptyView, R.string.no_connection, Snackbar.LENGTH_INDEFINITE);
							noConnectionSnackbar.setAction(R.string.retry, new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									loadMovie(movieId);
								}
							});
							noConnectionSnackbar.show();
//						} else {
//							// handle actual http return codes
						}
					}
				});

		AppController.getInstance().addToRequestQueue(request, this);
	}

	private void loadPoster(final ImageView view, final String posterPath) {
		String url = UrlHelper.getPosterUrl(UrlHelper.PosterSize.W185, posterPath);

		ImageLoader imageLoader = AppController.getInstance().getImageLoader();
		view.setImageBitmap(null);

		imageLoader.get(url, new ImageLoader.ImageListener() {
			@Override
			public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
				if (response.getBitmap() != null) {
					view.setImageBitmap(response.getBitmap());
				}
			}

			@Override
			public void onErrorResponse(VolleyError error) {
			}
		});
	}

	private void loadBackdrop(final ImageView view, final String backdropPath) {
		if (view == null) // backdrop might not be available on large displays
			return;

		String url = UrlHelper.getBackdropUrl(UrlHelper.BackdropSize.W1280, backdropPath);

		ImageLoader imageLoader = AppController.getInstance().getImageLoader();
		view.setImageBitmap(null);

		imageLoader.get(url, new ImageLoader.ImageListener() {
			@Override
			public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
				if (response.getBitmap() != null) {
					view.setImageBitmap(response.getBitmap());
					generateBackdropPalette(response.getBitmap());
				}
			}

			@Override
			public void onErrorResponse(VolleyError error) {
			}
		});
	}

	private void generateBackdropPalette(Bitmap bitmap) {
		Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
			@Override
			public void onGenerated(Palette palette) {
				int primaryColor = getResources().getColor(R.color.colorPrimary);
				int mutedColor = palette.getDarkVibrantColor(primaryColor);
				toolbarLayout.setContentScrimColor(mutedColor);
			}
		});
	}


	private void displayMovieView(MovieDetail movie) {
		if (movie == null || !isAdded())
			return;

		this.movie = movie;

		contentView.setVisibility(View.VISIBLE);
		emptyView.setVisibility(View.GONE);

		loadPoster(posterImageView, movie.getPoster_path());
		loadBackdrop(backdropImageView, movie.getBackdrop_path());

		try {
			Date date = serverDateFormat.parse(movie.getRelease_date());
			String releaseDateText = displayDateFormat.format(date);
			releaseDateTextView.setText(releaseDateText);
		} catch (ParseException e) {
			releaseDateTextView.setVisibility(View.GONE);
		}

		titleTextView.setText(movie.getTitle());
		overviewTextView.setText(movie.getOverview());
		taglineTextView.setText(movie.getTagline());
		runningTimeTextView.setText(getString(R.string.runtime, movie.getRuntime()));

		String genresText = "";
		List<Genre> genres = movie.getGenres();
		for (int i = 0; i < genres.size(); i++) {
			if (i > 0)
				genresText += ", ";
			genresText += genres.get(i).getName();
		}
		genresTextView.setText(genresText);

		if (toolbarLayout != null) {
			toolbarLayout.setTitle(movie.getTitle());
		}
	}

	private void displayEmptyView() {
		emptyView.setVisibility(View.VISIBLE);
	}


//	@Override
//	public Loader<Cursor> onCreateLoader(int loaderID, Bundle args) {
//		// TODO
//		return null;
//	}
//	@Override
//	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//		// TODO
//	}
//	@Override
//	public void onLoaderReset(Loader<Cursor> loader) {
//		// do nothing
//	}

}
