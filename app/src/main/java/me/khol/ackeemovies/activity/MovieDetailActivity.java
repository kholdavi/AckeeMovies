package me.khol.ackeemovies.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import me.khol.ackeemovies.R;


public class MovieDetailActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_movie_detail);

		setupToolbar();
		setupActionBarHomeAsUp();

		if (savedInstanceState == null) {
			int movieId = getIntent().getIntExtra(MovieDetailFragment.ARG_MOVIE_ID, -1);
			String movieTitle = getIntent().getStringExtra(MovieDetailFragment.ARG_MOVIE_TITLE);

			MovieDetailFragment fragment = MovieDetailFragment.newInstance(movieId, movieTitle);

			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.movie_detail_container, fragment)
					.commit();
		}
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
		setSupportActionBar(toolbar);
	}

	private void setupActionBarHomeAsUp() {
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				NavUtils.navigateUpTo(this, new Intent(this, MovieListActivity.class));
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
