package me.khol.ackeemovies.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import me.khol.ackeemovies.AppController;
import me.khol.ackeemovies.UrlHelper;
import me.khol.ackeemovies.R;
import me.khol.ackeemovies.adapter.MovieRecyclerViewAdapter;
import me.khol.ackeemovies.model.Movie;
import me.khol.ackeemovies.model.MoviesPage;


public class MovieListActivity extends AppCompatActivity implements MovieRecyclerViewAdapter.OnItemClickedListener {

	private static final String KEY_CURRENT_PAGE = "current_page";


	private RecyclerView recyclerView;
	private View emptyView;
	private Toolbar toolbar;

	private Snackbar noConnectionSnackbar;

	private boolean isTwoPane;
	private int currentPage = 1;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_movie_list);

		if (findViewById(R.id.movie_detail_container) != null) {
			isTwoPane = true;
		}

		setupToolbar();
		setupRecyclerView();

		if (savedInstanceState != null) {
			currentPage = savedInstanceState.getInt(KEY_CURRENT_PAGE);
		}

		loadPopularMovies(currentPage);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (noConnectionSnackbar != null)
			noConnectionSnackbar.dismiss();
	}

	private void setupToolbar() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setTitle(getTitle());
		toolbar.setSubtitle(getString(R.string.page, currentPage));
	}

	private void setupRecyclerView() {
		recyclerView = (RecyclerView) findViewById(R.id.movie_list);
		emptyView = findViewById(R.id.empty_view);
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_CURRENT_PAGE, currentPage);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.list_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
			case R.id.action_clear_caches:
				AppController.getInstance().getRequestQueue().getCache().clear();
				return true;

			case R.id.action_previous_page:
				if (currentPage > 1) {
					currentPage--;
					loadPopularMovies(currentPage);
				}
				return true;

			case R.id.action_next_page:
				currentPage++;
				loadPopularMovies(currentPage);
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}



	private void loadPopularMovies(int page) {
		toolbar.setSubtitle(getString(R.string.page, currentPage));
		String url = UrlHelper.getPopularMoviesUrl(page);

		if (noConnectionSnackbar != null)
			noConnectionSnackbar.dismiss();

		AppController.getInstance().cancelPendingRequests(this);

		JsonObjectRequest request = new JsonObjectRequest(url,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Gson gson = new Gson();

						String json = response.toString();
						MoviesPage moviesPage = gson.fromJson(json, MoviesPage.class);
						List<Movie> results = moviesPage.getResults();

						displayMovies(results);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						if (error.networkResponse == null) {
							noConnectionSnackbar = Snackbar.make(recyclerView, R.string.no_connection, Snackbar.LENGTH_INDEFINITE);
							noConnectionSnackbar.setAction(R.string.retry, new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									loadPopularMovies(currentPage);
								}
							});
							noConnectionSnackbar.show();
//						} else {
//							// handle actual http return codes
						}
						displayEmptyView();
					}
				});

		AppController.getInstance().addToRequestQueue(request, this);
	}

	private void displayEmptyView() {
		recyclerView.setVisibility(View.GONE);
		emptyView.setVisibility(View.VISIBLE);
	}

	private void displayMovies(List<Movie> results) {
		recyclerView.setVisibility(View.VISIBLE);
		emptyView.setVisibility(View.GONE);
		MovieRecyclerViewAdapter adapter = new MovieRecyclerViewAdapter(this, results);
		adapter.setOnItemClickedListener(this);
		recyclerView.setAdapter(adapter);
	}


	@Override
	public void onAdapterItemClicked(View v, Movie movie) {
		if (isTwoPane) {
			MovieDetailFragment fragment = MovieDetailFragment.newInstance(movie.getId(), movie.getTitle());

			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.movie_detail_container, fragment)
					.commit();
		} else {
			Context context = v.getContext();
			Intent intent = new Intent(context, MovieDetailActivity.class);
			intent.putExtra(MovieDetailFragment.ARG_MOVIE_ID, movie.getId());
			intent.putExtra(MovieDetailFragment.ARG_MOVIE_TITLE, movie.getTitle());

			context.startActivity(intent);
		}
	}


}
