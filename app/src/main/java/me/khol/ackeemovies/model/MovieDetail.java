package me.khol.ackeemovies.model;

import java.io.Serializable;
import java.util.List;


public class MovieDetail implements Serializable {

	private String backdrop_path;
	private List<Genre> genres;
	private String original_title;
	private String overview;
	private String poster_path;
	private String release_date;
	private String runtime;
	private String tagline;
	private String title;
	private String vote_average;


	public String getBackdrop_path() {
		return backdrop_path;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public String getOriginal_title() {
		return original_title;
	}

	public String getOverview() {
		return overview;
	}

	public String getPoster_path() {
		return poster_path;
	}

	public String getRelease_date() {
		return release_date;
	}

	public String getRuntime() {
		return runtime;
	}

	public String getTagline() {
		return tagline;
	}

	public String getTitle() {
		return title;
	}

	public String getVote_average() {
		return vote_average;
	}


	public void setBackdrop_path(String backdrop_path) {
		this.backdrop_path = backdrop_path;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public void setOriginal_title(String original_title) {
		this.original_title = original_title;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setPoster_path(String poster_path) {
		this.poster_path = poster_path;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setVote_average(String vote_average) {
		this.vote_average = vote_average;
	}
}
