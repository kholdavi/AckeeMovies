package me.khol.ackeemovies;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;


public class Provider extends ContentProvider  {


	private static final int MOVIES = 1;
	private static final int MOVIE_ID = 2;
	private static final int GENRES = 3;
	private static final int GENRE_ID = 4;

	private static final String AUTHORITY = "me.khol.ackeemovies.model";

	private static final String BASE_PATH_MOVIES = "movies";
	private static final String BASE_PATH_GENRES = "genres";

	public static final Uri CONTENT_URI_MOVIES = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_MOVIES);
	public static final Uri CONTENT_URI_GENRES = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_GENRES);

	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, BASE_PATH_MOVIES, MOVIES);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH_MOVIES + "/#", MOVIE_ID);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH_GENRES, GENRES);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH_GENRES + "/#", GENRE_ID);
	}



	@Override
	public boolean onCreate() {
		return false;
	}

	@Nullable
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		switch (sURIMatcher.match(uri)) {
			case MOVIE_ID:
				break;

			case MOVIES:
				break;

			case GENRE_ID:
				break;

			case GENRES:
				break;
		}
		return null;
	}

	@Nullable
	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Nullable
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}
}
